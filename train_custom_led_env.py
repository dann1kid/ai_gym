from stable_baselines3.common.evaluation import evaluate_policy
from MyEnv import MyEnv
from stable_baselines3.common.env_checker import check_env
from stable_baselines3 import PPO

# процедура обучения и сохранение модели на диск
from utils import create_dir
from settings import models_dir


def start_learning(my_env=None, models_dir=None):
    """
        Запускает обучение модели в среде
    :param models_dir:
    :param my_env: класс пользовательской среды
    :return: None
    """
    model = PPO("MlpPolicy", my_env, verbose=1, tensorboard_log="./Tensorboard_Robotics/")
    model.learn(total_timesteps=149504)

    model.save(f"{models_dir}/my_env")
    my_env.close()
    mean_reward, std_reward = evaluate_policy(model, my_env, n_eval_episodes=100)
    print(f"mean_reward:{mean_reward:.2f} +/- {std_reward:.2f}")

    return None


def main():
    print("Анализ среды...")
    create_dir(models_dir)
    print("Диагностический вывод")
    my_env = MyEnv()
    check_env(my_env)
    print("Начинаю обучение...")
    start_learning(my_env=my_env, models_dir=models_dir)
    print("Обучение завершено.")

    return None


if __name__ == '__main__':
    main()
