import cv2
import numpy as np


def get_frame_webcam(webcam):
    # читаем фреймы
    ret, frame = webcam.read()

    return frame


def create_mask(frame):
    # делаем маску под красный цвет
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # Выделяем маской цвета с наибольшим значением
    mask = (hsv[:, :, 0] > 175)
    # увеличиваем плотность пикселей одного цвета
    mask = mask.astype(np.uint8)

    return cv2.dilate(mask, np.ones((3, 3)))


def draw_center_circle(frame, frame_center_x, frame_center_y):
    # рисует круг с радиусом 15 пикселей в центре кадра
    radius = 15
    color = (255, 100, 25)
    thickness = 2
    center = (frame_center_x, frame_center_y)

    return cv2.circle(frame, center, radius, color, thickness)


def draw_rectangle(x, y, color=(255, 0, 255)):
    rect_start_point = (x - 50, y + 50)
    rect_end_point = (x + 50, y - 50)
    thickness = 5

    return rect_start_point, rect_end_point, color, thickness


def search_color_blob(masked_frame):
    # поиск самого большого массива
    # возвращает местоположение наибольшего массива цвета - x, y
    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(masked_frame)

    x, y = None, None
    max_area = None
    # ищем циклом самый большой массив
    for stat, center in zip(stats[1:], centroids[1:]):
        area = stat[4]

        if (max_area is None) or (area > max_area):
            # если сравниваемая зона больше предыдущей
            # перезаписывает коорды
            x, y = center
            max_area = area
    # в минус для выключения поиска на экране.
    # для функций поиска и рисования None это проблема
    if (x or y) is None:
        x, y = -100, -100

    return int(x), int(y)
