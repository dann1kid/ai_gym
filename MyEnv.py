import socket

import cv2
import gym
import numpy as np
from cv2 import imshow
from gym.spaces import Discrete, Dict

from camera_utils import get_frame_webcam, create_mask, search_color_blob, draw_center_circle, draw_rectangle
from settings import IP_RPI, PORT
from utils import generate_rand_point, send, generate_point


class MyEnv(gym.Env):
    """
    Класс среды для работы с SenseHat
    Алгоритм в общих чертах:
     1 сначала детектится позиция на камере,
     2 сопоставляется с позицией, отправленной клиентом к рпи,
       (здесь нужно бегать по массиву с позициями ледов)
     3 затем агент пытается выставить позицию согласно камере циклом
      по одному пикселу, пока не будет done со стороны среды,
     4 выясняя в какую сторону больший ревард,
     5 путем расчета расстояния от пикселя до центра кадра
     6 Переход на соседний пиксель должен вернуть ревард после сравнения с расстоянием.

     Не забудь подключить камеру и проверить ее доступность перед обучением!

     """

    def __init__(self):
        # Инициализация родительского класса
        super().__init__()
        # запускаем камеру
        self.webcam = cv2.VideoCapture(0)
        # кадр
        self.frame = None

        # центр кадра
        self.frame_center_x = int(self.webcam.get(3) / 2)
        self.frame_center_y = int(self.webcam.get(4) / 2)

        self.calculated_distance_curr_frame = (0, 0)
        self.calculated_distance_next_frame = (0, 0)

        # задаем границы арены шаблоном
        self.arena_space = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0]])

        # начальная позиция горящего леда, можно потом убрать
        self.led_x, self.led_y = 0, 0

        # слот под запоминание
        self.last_distance = 0
        # Дублируем шаблон для запоминания изменений
        self.last_led_state = np.copy(self.arena_space)

        # задаем среду
        self.score = Discrete(2)
        self.time_elapsed = Discrete(1)
        self.env = Dict(score=self.score,
                        time_elapsed=self.time_elapsed)
        # задаем действия перехода - 8. 4 в стороны + 4 по диагонали
        self.action_space = gym.spaces.Discrete(9)
        self.observation_space = gym.spaces.Box(low=-1, high=1, shape=(8, 8), dtype=np.float32)
        self.current_state = 0

        # настройки сети для трейн агента
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.s.connect((IP_RPI, PORT))

    def step(self, action):

        # Меняем состояние и вычисляем профит

        # 3 - проверяем разницу между стейтами (позицию найденного леда)
        # если все ок, добавляем ревард иначе ревард = 0
        err_msg = f"{action} - {type(action)} неверное"
        assert self.action_space.contains(action), err_msg
        # 1 - нейросетка пытается сделать шаг, и нужно его ограничить массивом арены
        # 1 мап действий, для понимания
        action_map = np.array([
            [0, 1, 2],
            [7, 8, 3],
            [6, 5, 4],
        ])
        # нужно как то сделать по формуле матрицы одна относительно другой
        # выясняем предыдущее состояние LED
        last_state_led_indexes = np.where(self.last_led_state == 1)
        last_led_x, last_led_y = last_state_led_indexes[0][0], last_state_led_indexes[1][0]
        # выясняем смещение относительно предыдущего состояния
        new_led_x = 0
        new_led_y = 0
        if action == 0:
            new_led_x = last_led_x - 1
            new_led_y = last_led_y - 1
        elif action == 1:
            new_led_x = last_led_x - 1
        elif action == 2:
            new_led_x = last_led_x - 1
            new_led_y = last_led_y + 1
        elif action == 3:
            new_led_y = last_led_y + 1
        elif action == 4:
            new_led_x = last_led_x + 1
            new_led_y = last_led_y + 1
        elif action == 5:
            new_led_x = last_led_x + 1
        elif action == 6:
            new_led_x = last_led_x + 1
            new_led_y = last_led_y - 1
        elif action == 7:
            new_led_y = last_led_y - 1
        elif action == 8:
            new_led_x = last_led_x
            new_led_y = last_led_y

        # выравниваем новую позицию что бы она не выходила за рамки
        if new_led_x < 0:
            new_led_x = 0
        elif new_led_x > 7:
            new_led_x = 7

        if new_led_y > 7:
            new_led_y = 7
        elif new_led_y < 0:
            new_led_y = 0

        # 2 - если все ок, берем текущий стейт, делаем шаг на LED сетке
        # если увеличение дистанции ревард = 0
        # теперь берем старое расстояние с фрейма,
        # 1 берем положение
        self.current_cam_state()
        # 2 расстояние
        self.calculate_current_distance()
        # устанавливаем новую позицию в сетке LED (отправляем на сервер)
        send(conn=self.s, data=generate_point(x=new_led_x, y=new_led_y))
        # и рисуем на кадре
        # Если фрейм получен,
        if self.frame is not None:
            edited_frame = draw_center_circle(self.frame, self.frame_center_x, self.frame_center_y)
            # показать окно с нарисованным кругом
            try:
                edited_frame = cv2.rectangle(edited_frame, *draw_rectangle(self.led_x, self.led_y))
            except Exception as e:
                print("Пиксель не найден")
            imshow("Train frame", edited_frame)
            cv2.waitKey(1)
        # и в массиве
        self.last_led_state[last_led_x][last_led_y] = 0
        self.last_led_state[new_led_x][new_led_y] = 1
        # берем новое расстояние
        self.current_cam_state()
        # вычислем новое расстояние
        self.calculate_new_distance(new_led_x, new_led_y)

        # 3 сравниваем и даем ревард
        # забыл про done. Если реварда нет,
        # то нейронка сходила не туда и ей нужно попробовать сходить еще раз
        done = False
        if self.calculated_distance_curr_frame[0] > self.calculated_distance_next_frame[0]:
            reward = 1.0
            done = True
        elif self.calculated_distance_curr_frame[1] > self.calculated_distance_next_frame[1]:
            reward = 1.0
            done = True
        elif (self.calculated_distance_curr_frame[0] > self.calculated_distance_next_frame[0]) and \
                self.calculated_distance_curr_frame[1] > self.calculated_distance_next_frame[1]:
            reward = 2.0
            done = True
        else:
            reward = 0.0

        info = {"prev_state": [last_led_x, last_led_y],
                "new_state": [new_led_x, new_led_y],
                "new_led_state": self.last_led_state,
                }

        return self.last_led_state, reward, done, info

    def reset(self):
        # Копируем состояние сетки для обсервации TODO: test it
        # observation = np.copy(self.last_led_state)
        # сбрасываем начальное состояние
        self.last_led_state = np.copy(self.arena_space)
        # генерируем новую позицию
        self.current_state = generate_rand_point()
        # устанавливаем на сетке новую случайную позицию
        x, y = self.current_state[0], self.current_state[1]
        self.last_led_state[x][y] = 1
        # отправляем на рпи
        send(conn=self.s, data=self.current_state)

        return self.last_led_state

    # код с вычислением координат не высчитывает реальное распроложение от точки до точки
    # он просто делает разницу по осям, чего уже достаточно для выяснения,
    # приближается ли горящий лед к центру или увеличивает расстояние
    # так же он пишет сразу в экземпляр класса, что упрощает доступ методам к информации через __dict__
    def current_cam_state(self):
        # вычисляет позицию светящегося леда с фрейма
        # возвращает позицию на кадре
        self.frame = get_frame_webcam(self.webcam)
        # делаем маску для поиска массива
        masked_frame = create_mask(self.frame)
        # поиск координат максимального цвета (x, y)
        self.led_x, self.led_y = search_color_blob(masked_frame)

    def calculate_current_distance(self):
        # вычисляет разницу в расстояниях между центром фрейма
        # и обнаруженным горящим ледом
        try:
            calculated_distance_x = self.frame_center_x - self.led_x
            calculated_distance_y = self.frame_center_y - self.led_y

            self.calculated_distance_curr_frame = abs(calculated_distance_x), abs(calculated_distance_y)
        except TypeError:
            print("Ошибка вычисление текущего фрейма")
            self.calculated_distance_curr_frame = None, None

    def calculate_new_distance(self, new_led_x=0, new_led_y=0):
        # вычисляет разницу в расстояниях между центром фрейма
        # и обнаруженным горящим ледом
        try:
            calculated_distance_x = self.frame_center_x - new_led_x
            calculated_distance_y = self.frame_center_y - new_led_y
            self.calculated_distance_next_frame = abs(calculated_distance_x), abs(calculated_distance_y)

        except TypeError:
            print("Ошибка вычисление следующего фрейма")
            self.calculated_distance_next_frame = None, None
