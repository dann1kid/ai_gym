import pickle
import random
import os


def generate_rand_point(max_x=7, max_y=7, r=255, g=0, b=0):
    """генерирует на двумерной матрице
    координату точки с указанным цветом
    dot = (x, y, 0,0,0)
    """
    x = random.randint(0, max_x)
    y = random.randint(0, max_y)
    return (x, y, r, g, b)


def generate_point(x=0, y=0, r=255, g=0, b=0):
    """генерирует на двумерной матрице
    координату точки с указанным цветом
    dot = (x, y, 0,0,0)
    """
    return (x, y, r, g, b)


def send(conn, data):
    try:
        data = pickle.dumps(data)
        conn.sendall(data)
    except Exception:
        raise


def recv(s):
    try:
        return pickle.loads(s.recv(1024))
    except Exception:
        raise


def create_dir(dir_name):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)

    return None
