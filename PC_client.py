import csv
import socket

import cv2
from cv2 import imshow
from stable_baselines3 import PPO
from stable_baselines3.common.env_util import make_vec_env

from MyEnv import MyEnv
from camera_utils import get_frame_webcam, create_mask, draw_rectangle, search_color_blob

from settings import IP_RPI, PORT, models_dir


# PC SIDE
from utils import generate_rand_point, send, recv


def save_data_row(name_sensor, row):
    """

    :param name_sensor: str
    :param row: list with data
    :return: None
    """
    with open(f"{name_sensor}.csv", "a", newline='') as f:
        writer = csv.writer(f)
        writer.writerow(row)

    return None


def main():
    # setup cam stream
    webcam = cv2.VideoCapture(0)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    s.connect((IP_RPI, PORT))
    # setup environment ???
    # env = MyEnv()
    # agent = PPO('MlpPolicy', env)
    # agent.learn(total_timesteps=1000000)

    while True:
        print("Connected")
        # принимаем данные
        data_rows = recv(s)
        print("Receiving", data_rows)
        # перечисляем сенсоры в принятых данных
        for sensor in data_rows:
            # и раздельно пишем каждый в отдельную таблицу
            values = data_rows[sensor]
            save_data_row(sensor, values.values())

        # генерация точки
        dot = generate_rand_point()
        # отправляем данные
        send(s, dot)

        done = None
        # Задаем вложенный цикл для работы агента
        while not done:
            # Обработка видеострима
            frame = get_frame_webcam(webcam)
            # делаем маску для поиска массива
            masked_frame = create_mask(frame)
            # поиск координат максимального цвета
            x, y = search_color_blob(masked_frame)

            # квадрат на фрейме по координатам после поиска
            frame = cv2.rectangle(frame, *draw_rectangle(x, y))

            # грузим сетку
            model = PPO.load(f"{models_dir}/my_env")
            my_env = MyEnv
            env = make_vec_env(my_env, n_envs=1)
            # сброс
            obs = env.reset()
            # делаем предикт
            action, _states = model.predict(obs)
            obs, rewards, done, info = env.step(action)

            # Показываем конечный результат
            imshow("Edited", frame)
            if cv2.waitKey(1) and 0xFF == ord("q"):
                break


if __name__ == '__main__':
    main()
